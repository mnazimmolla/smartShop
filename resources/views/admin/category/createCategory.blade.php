@extends('admin.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<br>
		<h3>{{ Session::get('message') }}</h3>
		<div class="">
			{!! Form::open(['url' => 'category/save', 'methos' => 'POST']) !!}
			<div class="form-group">
				<label for="categoryName">Category Name</label>
				<input type="text" name="categoryName" id="categoryName" class="form-control" placeholder="Category Name">
				<span class="text-danger">{{ $errors->has('categoryName')? $errors->first('categoryName'): '' }}</span>
			</div>
			<div class="form-group">
				<label for="categoryDescription">Category Description</label>
				<textarea name="categoryDescription" id="categoryDescription" rows="5" class="form-control" placeholder="Category Description"> </textarea>
				<span class="text-danger">{{ $errors->has('categoryDescription')? $errors->first('categoryDescription'): '' }}</span>
			</div>
			<div class="form-group">
				<label for="categoryStatus">Category Status</label>
				<select class="form-control" name="categoryStatus" id="categoryStatus"> 
					<option for="">Category Status</option>
					<option value="1">Published</option>
					<option value="0">Unpublished</option>					
				</select>
			</div>
			<button type="submit" name="btn" class="btn btn-primary btn-block">Save Category</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection