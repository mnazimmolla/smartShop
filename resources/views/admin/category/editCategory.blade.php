@extends('admin.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<br>
		<div class="">
			{!! Form::open(['url' => 'category/update', 'methos' => 'POST', 'name' => 'editCategoryForm']) !!}
			<div class="form-group">
				<label for="categoryName">Category Name</label>
				<input type="text" name="categoryName" id="categoryName" class="form-control" value="{{ $categoryById->categoryName }}" placeholder="Category Name">
				<input type="hidden" name="id" value="{{ $categoryById->id }}">
			</div>
			<div class="form-group">
				<label for="categoryDescription">Category Description</label>
				<textarea name="categoryDescription" id="categoryDescription" rows="5" class="form-control" placeholder="Category Description">{{ $categoryById->categoryDescription }}</textarea>
			</div>
			<div class="form-group">
				<label for="categoryStatus">Category Status</label>
				<select class="form-control" name="categoryStatus" id="categoryStatus"> 
					<option for="">Category Status</option>
					<option value="1">Published</option>
					<option value="0">Unpublished</option>					
				</select>
			</div>
			<button type="submit" name="btn" class="btn btn-primary btn-block">Update Category</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">
	document.forms['editCategoryForm'].elements['categoryStatus'].value={{ $categoryById->categoryStatus }}
</script>
@endsection