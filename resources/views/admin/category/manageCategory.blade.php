@extends('admin.master')
@section('content')
<br>
<br>
@if(Session::get('message') == true)
<h3 class="text-center text-success">{{ Session::get('message') }}</h3>
@endif
<table class="table table-stripe table-bordered">
	<tr>
		<td>Name</td>
		<td>Description</td>
		<td>Status</td>
		<td>Action</td>
	</tr>
@foreach($category as $value)
	<tr>
		<td>{{ $value->categoryName }}</td>
		<td>{{ $value->categoryDescription }}</td>
		<td>{{ $value->categoryStatus == 1 ? 'Published' : 'Unpublised' }}</td>
		<td>
			<a href="{{ url('/category/edit/' . $value->id) }}" class="btn btn-success">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="{{ url('/category/delete/' . $value->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
@endforeach
</table>
<div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
    {{$category->links()}}
</div>
@endsection