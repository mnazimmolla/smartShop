@extends('admin.master')
@section('content')
<br>
<br>
@if(Session::get('message') == true)
<h3 class="text-center text-success">{{ Session::get('message') }}</h3>
@endif
<a href="{{ url('/product/add') }}" class="btn btn-success">Add Product</a>
<br>
<br>
<table class="table table-stripe table-bordered">
	<tr>
		<td>Name</td>
		<td>Category</td>
		<td>Manufaturer</td>
		<td>Price</td>
		<td>Quantity</td>
		<td>Status</td>
		<td>Action</td>
	</tr>
@foreach($products as $product)
	<tr>
		<td>{{ $product->productName }}</td>
		<td>{{ $product->categoryName }}</td>
		<td>{{ $product->manufacturerName }}</td>
		<td>{{ $product->productPrice }}</td>
		<td>{{ $product->productQuantity }}</td>
		<td>{{ $product->publicationStatus == 1 ? 'Published' : 'Unpublised' }}</td>
		<td>
			<a href="{{ url('/product/view/' . $product->id) }}" class="btn btn-info" title="View Product">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
			<a href="{{ url('/product/edit/' . $product->id) }}" class="btn btn-success" title="Edit Product">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="{{ url('/product/delete/' . $product->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')" title="Delete Product">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
@endforeach
</table>
<div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
    {{$products->links()}}
</div>
@endsection