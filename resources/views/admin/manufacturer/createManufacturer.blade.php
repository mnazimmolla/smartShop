@extends('admin.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<br>
		@if(Session::has('message'))
			<h3 class="text-center text-seccess">{{ Session::get('message') }}</h3>
		@endif
		<div class="">
			{!! Form::open(['url' => 'manufacturer/save', 'methos' => 'POST']) !!}
			<div class="form-group">
				<label for="manufacturerName">Manufacturer Name</label>
				<input type="text" name="manufacturerName" id="manufacturerName" class="form-control" placeholder="Manufacturer Name">
				<span class="text-danger">{{ $errors->has('manufacturerName')? $errors->first('manufacturerName'): '' }}</span>
			</div>
			<div class="form-group">
				<label for="Description">Manufacturer Description</label>
				<textarea name="manufacturerDescription" id="manufacturerDescription" rows="5" class="form-control" placeholder="Manufacturer Description"> </textarea>
				<span class="text-danger">{{ $errors->has('manufacturerDescription')? $errors->first('manufacturerDescription'): '' }}</span>
			</div>
			<div class="form-group">
				<label for="manufacturerStatus">Manufacturer Status</label>
				<select class="form-control" name="manufacturerStatus" id="manufacturerStatus"> 
					<option for="">Manufacturer Status</option>
					<option value="1">Published</option>
					<option value="0">Unpublished</option>					
				</select>
			</div>
			<button type="submit" name="btn" class="btn btn-primary btn-block">Save Category</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection