
@extends('admin.master')
@section('content')
<br>
<br>
@if(Session::get('message') == true)
<h3 class="text-center text-success">{{ Session::get('message') }}</h3>
@endif
<table class="table table-stripe table-bordered">
	<tr>
		<td>Name</td>
		<td>Description</td>
		<td>Status</td>
		<td>Action</td>
	</tr>
@foreach($manufacturers as $value)
	<tr>
		<td>{{ $value->manufacturerName }}</td>
		<td>{{ $value->manufacturerDescription }}</td>
		<td>{{ $value->manufacturerStatus == 1 ? 'Published' : 'Unpublised' }}</td>
		<td>
			<a href="{{ url('/manufacturer/edit/' . $value->id) }}" class="btn btn-success">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="{{ url('/manufacturer/delete/' . $value->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
@endforeach
</table>
<div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
    {{$manufacturers->links()}}
</div>
@endsection