@extends('admin.master')
@section('content')
<br>
<br>
@if(Session::get('message') == true)
<h3 class="text-center text-success">{{ Session::get('message') }}</h3>
@endif
<a href="{{ url('/student/add') }}" class="btn btn-success">Add Student</a>
<br>
<br>
<table class="table table-stripe table-bordered">
	<tr>
		<td>Name</td>
		<td>E-mail</td>
		<td>Department</td>
		<td>Action</td>
	</tr>
@foreach($students as $student)
	<tr>
		<td>{{ $student->name }}</td>
		<td>{{ $student->email }}</td>
		<td>{{ @substr($student->department, 0, 10) }}</td>
		<td>
			<a href="{{ url('/student/view/' . $student->id) }}" class="btn btn-info" title="View Product">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
			<a href="{{ url('/student/edit/' . $student->id) }}" class="btn btn-success" title="Edit Product">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="{{ url('/student/delete/' . $student->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')" title="Delete Product">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
@endforeach
    <tr>
      <td colspan="3">
        <center> <!-- lol -->
          <button class="btn btn-default" ic-get-from="/contacts/?page=2" ic-target="closest tr" ic-replace-target="true">
            Load More Agents... <i class="fa fa-spinner fa-spin ic-indicator" style="display:none"></i>
          </button>
        </center>
      </td>
    </tr>
</table>
<div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
   {{$students->links()}} 
</div>

<!-- Ajax -->

  <table class="table">

    <tbody id="contactTableBody">
    <tr>
      <td>Agent Smith</td><td>void0@null.org</td><td>D92B582CA8GDD94</td>
    </tr>

    </tbody>
  </table>

  <script>

    //========================================================================
    // Mock Server-Side HTTP End Point
    //========================================================================
    $.mockjax({
      url: /\/students.*/,
      responseTime: 800,
      response: function (settings) {
        var queryStr = settings.url.substring(settings.url.lastIndexOf('?'));
        var params = parseParams(queryStr);

        var page = parseInt(params['page']);
        var contacts = dataStore.contactsForPage(page)
        this.responseText = rowsTemplate(page, contacts);
      }
    });

    //========================================================================
    // Mock Server-Side Templates
    //========================================================================
    function rowsTemplate(page, contacts) {
      var txt = "";
      for (var i = 0; i < contacts.length; i++) {
        var c = contacts[i];
        txt += "<tr> \
                  <td>" + c.name + "</td><td>" + c.email + "</td><td>" + c.department + "</td> \
                </tr>";
      }
      txt += loadMoreRow(page);
      return txt;
    }

    function loadMoreRow(page) {
      return '<tr id="replaceMe"> \
                <td colspan="3"> \
                  <center> \
                   <button class="btn btn-default"   ic-post-to="/students/?page=' + (page + 1) + '" ic-target="closest tr" ic-replace-target="true">\
                     Load More Agents... <i class="fa fa-spinner fa-spin ic-indicator" style="display:none"></i> \
                   </button> \
                   </center>\
                 </td>\
               </tr>';
    }

    //========================================================================
    // Mock Data Store
    //========================================================================
    var dataStore = function(){
      var contactId = 9;
      function generateContact() {
        contactId++;
        var idHash = "";
        var possible = "ABCDEFG0123456789";
        for( var i=0; i < 15; i++ ) idHash += possible.charAt(Math.floor(Math.random() * possible.length));
        return { name: "Agent Smith", email: "void" + contactId + "@null.org", id: idHash }
      }
      return {
        contactsForPage : function(page) {
          var vals = [];
          for( var i=0; i < 10; i++ ){
            vals.push(generateContact());
          }
          return vals;
        }
      }
    }()


  </script>
  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
  <script src="https://intercoolerreleases-leaddynocom.netdna-ssl.com/intercooler-1.2.1.min.js"></script>
<!-- Ajax -->
@endsection