@extends('admin.master')
@section('content')

<br>
<br>
<a href="{{ url('/product/manage') }}" class="btn btn-success">View Products</a>
<br>
<br>
<table class="table table-stripe table-bordered">
	<tr>
		<th>Product ID</th>
		<td>{{ $product->id }}</td>
	</tr>
	<tr>
		<th>Name</th>
		<td>{{ $product->productName }}</td>
	</tr>
	<tr>
		<th>Image</th>
		<td>
		<img class="img-productbyid" src="{{ asset('public/productImage'). '/' . $product->productImage }}"/>
		</td>
	</tr>
	<tr>
		<th>Category</th>
		<td>{{ $product->categoryName }}</td>
	</tr>
	<tr>
		<th>Manufactuer</th>
		<td>{{ $product->manufacturerName }}</td>
	</tr>
	<tr>
		<th>Price</th>
		<td>{{ $product->productPrice }}</td>
	</tr>
	<tr>
		<th>Qunatity</th>
		<td>{{ $product->productQuantity }}</td>
	</tr>
	<tr>
		<th>Status</th>
		<td>{{ $product->publicationStatus == 1 ? 'Published' : 'Unpublised' }}</td>
	</tr>
	<tr>
		<th>Short Description</th>
		<td>{!! $product->productShortDescription !!}</td>
	</tr>
	<tr>
		<th>Short Description</th>
		<td>{!! $product->productLongDescription !!}</td>
	</tr>
	<tr>
		<th>Action</th>
		<td>
			<a href="{{ url('/product/edit/' . $product->id) }}" class="btn btn-success" title="Edit Product">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="{{ url('/product/delete/' . $product->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')" title="Delete Product">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
</table>
@endsection
