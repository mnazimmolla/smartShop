@extends('admin.master')
@section('content')
<div class="row">
	<div class="col-md-12">
		<br>
		@if(Session::has('message'))
			<h3 class="text-success text-center">{{ Session::get('message') }}</h3>
		@endif
		<a href="{{ url('/product/manage') }}" class="btn btn-success">View Products</a>
		<br>
		<br>
		<div class="">
			{!! Form::open(['url' => 'product/update', 'name' => 'editProductForm', 'methos' => 'POST', 'enctype' => 'multipart/form-data']) !!}
			<div class="form-group">
				<label for="productName">Product Name</label>
				<input type="text" name="productName" id="productName" class="form-control" placeholder="Product Name" value="{{ $product->productName }}">
				<span class="text-danger">{{ $errors->has('productName')? $errors->first('productName'): '' }}</span>
			</div>
			<input type="hidden" name="id" value="{{ $product->id }}">
			<div class="form-group">
				<label for="categoryId">Category Name</label>
				<select class="form-control" name="categoryId" id="categoryId"> 
					<option selected disabled="" for="">Select Category Name</option>
					@foreach($categories as $category)
						<option value="{{ $category->id }}">{{ $category->categoryName }}</option>
					@endforeach				
				</select>
			</div>
			<div class="form-group">
				<label for="manufacturerId">Manufacturer Name</label>
				<select class="form-control" name="manufacturerId" id="manufacturerId"> 
					<option selected disabled="" for="">Select Category Name</option>
					@foreach($manufacturers as $manufacturer)
						<option value="{{ $manufacturer->id }}">{{ $manufacturer->manufacturerName }}</option>
					@endforeach				
				</select>
			</div>
			<div class="form-group">
				<label for="productPrice">Product Price</label>
				<input type="number" name="productPrice" id="productPrice" class="form-control" placeholder="Product Price" value="{{ $product->productPrice }}">
				<span class="text-danger">{{ $errors->has('productPrice')? $errors->first('productPrice'): '' }}</span>
			</div>
			<div class="form-group">
				<label for="productQuantity">Product Quantity</label>
				<input type="number" name="productQuantity" id="productQuantity" class="form-control" placeholder="Product Quantity" value="{{ $product->productQuantity }}">
				<span class="text-danger">{{ $errors->has('productQuantity')? $errors->first('productQuantity'): '' }}</span>
			</div>						
			<div class="form-group">
				<label for="productShortDescription">Product Short Description</label>
				<textarea name="productShortDescription" id="productShortDescription" rows="5" class="form-control" placeholder="Product Short Description">{{ $product->productShortDescription }}</textarea>
				<span class="text-danger">{{ $errors->has('productShortDescription')? $errors->first('productShortDescription'): '' }}</span>
			</div>						
			<div class="form-group">
				<label for="productLongDescription">Product Long Description</label>
				<textarea name="productLongDescription" id="productLongDescription" rows="5" class="form-control" placeholder="Product Long Description">{{ $product->productLongDescription }}</textarea>
				<span class="text-danger">{{ $errors->has('productLongDescription')? $errors->first('productLongDescription'): '' }}</span>
			</div>			
			<div class="form-group">
				<p><b>Product Image</b></p>
				<img class="img-edit-page" src="{{ asset('public/productImage') . '/' .$product->productImage }}" alt="Product Image">
			</div>
			<div class="form-group">
				<label for="productImage">Product Image</label>
				<input type="file" accept="image/*" name="productImage" id="productImage" class="form-control" placeholder="Product Image">
				<span class="text-danger">{{ $errors->has('productImage')? $errors->first('productImage'): '' }}</span>
			</div>
			<div class="form-group">
				<label for="publicationStatus">Publication Status</label>
				<select class="form-control" name="publicationStatus" id="publicationStatus"> 
					<option for="publicationStatus">Publication Status</option>
					<option value="1">Published</option>
					<option value="0">Unpublished</option>					
				</select>
			</div>
			<button type="submit" name="btn" class="btn btn-primary btn-block">Save Product</button>
			{!! Form::close() !!}
		</div>
	</div>
</div>
<script type="text/javascript">
	document.forms['editProductForm'].elements['publicationStatus'].value={{ $product->publicationStatus }}
	document.forms['editProductForm'].elements['categoryId'].value={{ $product->categoryId }}
	document.forms['editProductForm'].elements['manufacturerId'].value={{ $product->manufacturerId }}
</script>
@endsection