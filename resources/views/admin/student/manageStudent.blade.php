@extends('admin.master')
@section('content')
<br>
<br>
@if(Session::get('message') == true)
<h3 class="text-center text-success">{{ Session::get('message') }}</h3>
@endif
<a href="{{ url('/student/add') }}" class="btn btn-success">Add Student</a>
<br>
<br>
<table class="table table-stripe table-bordered">
	<tr>
		<td>Name</td>
		<td>E-mail</td>
		<td>Department</td>
		<td>Action</td>
	</tr>
@foreach($students as $student)
	<tr>
		<td>{{ $student->name }}</td>
		<td>{{ $student->email }}</td>
		<td>{{ @substr($student->department, 0, 10) }}</td>
		<td>
			<a href="{{ url('/student/view/' . $student->id) }}" class="btn btn-info" title="View Product">
				<span class="glyphicon glyphicon-eye-open"></span>
			</a>
			<a href="{{ url('/student/edit/' . $student->id) }}" class="btn btn-success" title="Edit Product">
				<span class="glyphicon glyphicon-edit"></span>
			</a>
			<a href="{{ url('/student/delete/' . $student->id) }}" class="btn btn-danger" onclick="return confirm('are you sure?')" title="Delete Product">
				<span class="glyphicon glyphicon-trash"></span>
			</a>
		</td>
	</tr>
@endforeach
</table>
<div class="panel-heading" style="display:flex; justify-content:center;align-items:center;">
   {{$students->links()}} 
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js"></script>
<script type="text/javascript">
	var app = new vue();
</script>
@endsection