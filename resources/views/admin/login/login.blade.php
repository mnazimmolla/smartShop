<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin Login</title>

    <link href="{{ asset('public/admin/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/admin/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">
    <link href="{{ asset('public/admin/dist/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('public/admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">


</head>

<body>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        {!! Form::open(['url' => '/login']) !!}
                                <div class="form-group">
                                    {{ Form::label('E-mail') }}
                                    {{  Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Enter Your E-mail']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::label('password') }}
                                    {{  Form::password('password', ['class' => 'form-control', 'placeholder' => 'Enter Your Password']) }}                                
                                </div>
                                <div class="checkbox">
                                    <label>{{ Form::checkbox('name', 'rememberMe') }} Remember Me </label>
                                </div>
                                <div class="form-group">
                                    {{ Form::submit('Submit', ['class' => 'btn btn-success btn-block', 'name' => 'btn']) }}
                                </div>                                
                            
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('public/admin/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('public/admin/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('public/admin/vendor/metisMenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('public/admin/dist/js/sb-admin-2.js') }}"></script>

</body>

</html>
