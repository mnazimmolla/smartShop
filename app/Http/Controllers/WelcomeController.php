<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
class WelcomeController extends Controller
{
    public function index()
    {
        $publishedProduct = Product::where('publicationStatus', 1)->get();
        return view('frontEnd.home.homeContent', ['publishedProduct' => $publishedProduct]);
    	//return view('frontEnd.home.homeContent')->with('publishedProduct', $publishedProduct);
    }
    public function category($id)
    {
        $categoryInfo = Product::where('categoryId', $id)->where('publicationStatus', 1)->get();
    	return view('frontEnd.category.categoryContent', ['categoryInfo' => $categoryInfo]);
    }
    public function productDetails($id)
    {
        $productDetails = Product::find($id);
    	return view('frontEnd.product.productContent')->with('productDetails', $productDetails);
    }
}
