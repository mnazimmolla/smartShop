<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use DB;
class CategoryController extends Controller
{
    public function createCategory()
    {
    	return view('admin.category.createCategory');
    }
    public function storeCategory(Request $request)
    {
    	// $category = new Category();
    	// $category->categoryName = $request->categoryName;
    	// $category->categoryDescription = $request->categoryDescription;
    	// $category->categoryStatus = $request->categoryStatus;
    	// $category->save();

    	//Category::create($request->all());

        $this->validate($request, [
            'categoryName' => 'required',
            'categoryDescription' => 'required',
        ]);

    	DB::table('categories')->insert([
    		'categoryName' => $request->categoryName,
    		'categoryDescription' => $request->categoryDescription,
    		'categoryStatus' => $request->categoryStatus
    	]);
    	// return redirect()->back('message', 'category create successfull!');
    	return redirect('/category/add')->with('message', 'Category create successfull!');
    }
    public function manageCategory()
    {
        $category = Category::paginate(10);
        return view('admin.category.manageCategory', ['category' => $category]);
    }
    public function editCategory($id)
    {
        $categoryById = Category::where('id', $id)->first();
        return view('admin.category.editCategory', ['categoryById' => $categoryById]);
    }
    public function updateCategory(Request $request)
    {
        $category = Category::find($request->id);
        $category->categoryName = $request->categoryName;
        $category->categoryDescription = $request->categoryDescription;
        $category->categoryStatus = $request->categoryStatus;
        $category->save();
        return redirect('/category/manage')->with('message', 'Category update succeessfull!');
    }
    public function deleteCategory($id)
    {
        $category = Category::find($id);
        $category->delete();
        return redirect('/category/manage')->with('message', 'Category delete successfull!');
    }
}
