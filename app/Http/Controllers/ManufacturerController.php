<?php

namespace App\Http\Controllers;
use App\Manufacturer;
use Illuminate\Http\Request;
use DB;
class ManufacturerController extends Controller
{
    public function createManufacturer()
    {
        return view('admin.manufacturer.createManufacturer');
    }
    public function storeManufacturer(Request $request)
    {
        $this->validate($request, [
            'manufacturerName' => 'required',
            'manufacturerDescription' => 'required',
            'manufacturerStatus' => 'required',
        ]);
        DB::table('manufacturers')->insert([
            'manufacturerName' => $request->manufacturerName,
            'manufacturerDescription' => $request->manufacturerDescription,
            'manufacturerStatus' => $request->manufacturerStatus,
        ]);
        return redirect('/manufacturer/add')->with('message', 'Manufacturer create successful');
    }
    public function manageManufacturer()
    {
        $all = Manufacturer::paginate(10);
        return view('admin.manufacturer.manageManufacturer')->with('manufacturers', $all);
    }
    public function editManufacturer($id)
    {
        $manufacturer =  Manufacturer::where('id', $id)->first();
        return view('admin.manufacturer.editManufacturer')->with('manufacturer', $manufacturer);
    }
    public function updateManufacturer(Request $request)
    {
        $manufacturer = Manufacturer::find($request->id);
        $manufacturer->manufacturerName = $request->manufacturerName;
        $manufacturer->manufacturerDescription = $request->manufacturerDescription;
        $manufacturer->manufacturerStatus = $request->manufacturerStatus;
        $manufacturer->save();
        return redirect('manufacturer/manage/')->with('message', 'Manufacturer update successful!');
    }
    public function deleteManufacturer($id)
    {
        $manufacturer = Manufacturer::find($id);
        $manufacturer->delete();
        return redirect('/manufacturer/manage/')->with('message', 'Manufacturer delete successful!');
    }
}
