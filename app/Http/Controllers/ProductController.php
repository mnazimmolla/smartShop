<?php

namespace App\Http\Controllers;
use App\Category;
use App\Manufacturer;
use App\Product;
use DB;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function createProduct()
    {
    	$categories = Category::where('categoryStatus', 1)->get();
    	$manufacturers = Manufacturer::where('manufacturerStatus', 1)->get();
    	return view('admin.product.createProduct', ['categories' => $categories, 'manufacturers' => $manufacturers]);
    }
    public function storeProduct(Request $request)
    {
    	$this->validate($request, [
    		'productName' => 'required|min:6',
    		'productPrice' => 'required|integer',
    		'productQuantity' => 'required|integer',
    		'productShortDescription' => 'required|min:20',
    		'productLongDescription' => 'required|min:50',
    		'publicationStatus' => 'required|integer',
    	]);

    	//Image Processing
    	$productImage = $request->file('productImage');
    	$productImageName = $productImage->getClientOriginalName();
    	$validImageName = substr($productImageName, strpos($productImageName, ".") +1);
    	$validExt = array('jpg','png', 'PNG', 'gif','jpeg');
        if(!in_array($validImageName, $validExt))
        {
            return redirect('/product/add')->with('message', 'Invalid image format');
        } 	
    	$uploadPath = 'public/productImage/';
    	$requestName = $request->productName;
    	$requestName = explode(' ', $requestName);
    	$requestName = implode($requestName, '_');
    	$productImageName = str_replace($productImageName, $requestName, $productImageName);
    	$replacedName = $productImageName. '.' . $validImageName;
    	$productImage->move($uploadPath, $replacedName);
    	//Image Processing

        $this->saveProduct($request, $replacedName);
        return redirect('/product/add')->with('message', 'Product save successfully!');
    }
    public function saveProduct($request, $replacedName)
    {
        $product = new Product();
        $product->productName = $request->productName;
        $product->categoryId = $request->categoryId;
        $product->manufacturerId = $request->manufacturerId;
        $product->productPrice = $request->productPrice;
        $product->productQuantity = $request->productQuantity;
        $product->productShortDescription = $request->productShortDescription;
        $product->productLongDescription = $request->productLongDescription;
        $product->publicationStatus = $request->publicationStatus;
        $product->productImage = $replacedName;
        $product->save();
    }
    public function manageProduct()
    {
        $products = DB::table('products')
                ->join('categories', 'products.categoryId', '=', 'categories.id')
                ->join('manufacturers', 'products.manufacturerId', '=', 'manufacturers.id')
                ->select('products.*', 'manufacturers.manufacturerName', 'categories.categoryName')
                ->paginate(10);      
        return view('admin.product.manageProduct', ['products' => $products]);
    }
    public function viewProduct($id)
    {
        $product = DB::table('products')
                ->join('categories', 'products.categoryId', '=', 'categories.id')
                ->join('manufacturers', 'products.manufacturerId', '=', 'manufacturers.id')
                ->select('products.*', 'manufacturers.manufacturerName', 'categories.categoryName')
                ->where('products.id', $id)
                ->first();
        return view('admin.product.viewProduct', ['product' => $product]);
    }
    public function editProduct($id)
    {
        $categories = Category::where('categoryStatus', 1)->get();
        $manufacturers = Manufacturer::where('manufacturerStatus', 1)->get();
        $product = DB::table('products')
                ->join('categories', 'products.categoryId', '=', 'categories.id')
                ->join('manufacturers', 'products.manufacturerId', '=', 'manufacturers.id')
                ->select('products.*', 'manufacturers.manufacturerName', 'categories.categoryName')
                ->where('products.id', $id)
                ->first();
        return view('admin.product.editProduct', ['product' => $product, 'categories' => $categories, 'manufacturers' => $manufacturers ]);
    }
    public function updateProduct(Request $request)
    {
        $this->validate($request, [
            'productName' => 'required|min:6',
            'productPrice' => 'required|integer',
            'productQuantity' => 'required|integer',
            'productShortDescription' => 'required|min:20',
            'productLongDescription' => 'required|min:50',
            'publicationStatus' => 'required|integer',
        ]);

        //Image Processing
        $productImage = $request->file('productImage');
        if($productImage != null) {
            $productImageName = $productImage->getClientOriginalName();
            $validImageName = substr($productImageName, strpos($productImageName, ".") +1);
            $validExt = array('jpg','png', 'PNG', 'gif','jpeg');
            if(!in_array($validImageName, $validExt))
            {
                return redirect('/product/edit/'. $request->id)->with('message', 'Invalid image format');
            }   
            $uploadPath = 'public/productImage/';
            $requestName = $request->productName;
            $requestName = explode(' ', $requestName);
            $requestName = implode($requestName, '_');
            $productImageName = str_replace($productImageName, $requestName, $productImageName);
            $replacedName = $productImageName. '.' . $validImageName;
            $productImage->move($uploadPath, $replacedName);
            //Image Processing

            $this->updateSingleProduct($request, $replacedName);            
        }
        else
        {
            $this->updateSingleProduct($request, $replacedName='');
        }
        
        return redirect('/product/edit/' .$request->id )->with('message', 'Product update successfully!');
    }    
    public function updateSingleProduct($request, $replacedName)
    {
        $product = Product::find($request->id);
        $product->productName = $request->productName;
        $product->categoryId = $request->categoryId;
        $product->manufacturerId = $request->manufacturerId;
        $product->productPrice = $request->productPrice;
        $product->productQuantity = $request->productQuantity;
        $product->productShortDescription = $request->productShortDescription;
        $product->productLongDescription = $request->productLongDescription;
        $product->publicationStatus = $request->publicationStatus;
        if($replacedName != null) {
           $product->productImage = $replacedName; 
        }
        
        $product->save();
    }
    public function deleteProduct($id)
    {
        $product = Product::find($id);
        $product->delete();
        return redirect('product/manage/')->with('message', 'Product delete successfully!');
    }
}
