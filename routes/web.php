<?php

Route::get('/', 'WelcomeController@index');
Route::get('/category/{id}', 'WelcomeController@category');
Route::get('/product-details/{id}', 'WelcomeController@productDetails');
Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

// Categoty Info 
Route::get('/category/add', 'CategoryController@createCategory');
Route::post('/category/save', 'CategoryController@storeCategory');
Route::get('/category/manage', 'CategoryController@manageCategory');
Route::get('/category/edit/{id}', 'CategoryController@editCategory');
Route::post('/category/update/', 'CategoryController@updateCategory');
Route::get('/category/delete/{id}', 'CategoryController@deleteCategory');
// Categoty Info 

//Manufucturer
Route::get('/manufacturer/add', 'ManufacturerController@createManufacturer');
Route::post('manufacturer/save', 'ManufacturerController@storeManufacturer');
Route::get('manufacturer/manage', 'ManufacturerController@manageManufacturer');
Route::get('manufacturer/edit/{id}', 'ManufacturerController@editManufacturer');
Route::post('manufacturer/update/', 'ManufacturerController@updateManufacturer');
Route::get('manufacturer/delete/{id}', 'ManufacturerController@deleteManufacturer');
//Manufucturer

//Product
Route::get('/product/add', 'ProductController@createProduct');
Route::post('product/save', 'ProductController@storeProduct');
Route::get('product/view/{id}', 'ProductController@viewProduct');
Route::get('product/manage', 'ProductController@manageProduct');
Route::get('product/edit/{id}', 'ProductController@editProduct');
Route::post('product/update/', 'ProductController@updateProduct');
Route::get('product/delete/{id}', 'ProductController@deleteProduct');
//Product

Route::get('/data_maker',function(){
    $faker = Faker\Factory::create();

    $limit = 10;

    for ($i = 0; $i < $limit; $i++) {
        DB::table('students')->insert([
            'name' => $faker->name,
            'email' => $faker->email,
            'department' => $faker->text,
        ]);       
    }
});

//Intercoler JS
Route::get('/students', 'StudentController@index');