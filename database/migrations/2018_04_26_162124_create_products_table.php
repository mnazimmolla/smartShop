<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productName');
            $table->tinyinteger('categoryId');
            $table->tinyinteger('manufacturerId');
            $table->float('productPrice', 10, 2);
            $table->integer('productQuantity');
            $table->text('productShortDescription');
            $table->text('productLongDescription');
            $table->string('productImage');
            $table->tinyinteger('publicationStatus');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
